cmake_minimum_required(VERSION 3.6)
project(model LANGUAGES CXX DESCRIPTION "MOS T-16 building model")

set(CMAKE_CXX_STANDARD 23)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_executable(model main.cpp)

find_package(PkgConfig REQUIRED)

pkg_check_modules(piwo REQUIRED piwo>=1.10)
target_link_libraries(model piwo)
target_include_directories(model PRIVATE ${piwo_INCLUDE_DIRS})

pkg_check_modules(mipc REQUIRED mipc>=0.22)
target_link_libraries(model mipc)
target_include_directories(model PRIVATE ${mipc_INCLUDE_DIRS})
