#include <getopt.h>
#include <stdio.h>
#include <cstdint>
#include <string_view>

#include <unistd.h>
#include <fcntl.h>

static void
show_menu_options()
{
  const char* menu =
    "  --help   | -h             Show this message\n"
    "  --device | -d             Set output device (for example /dev/ttyACM0)\n\n"

    "  Provide path to animation in *.piwo7 format\n"
    "  Audio file must have same name as animation and be in *.mp3 format\n"
    "  If *.mp3 is not found, animation will be played without audio\n\n"

    "  To run this program you do not need to specify device, unless you are\n"
    "  not using properly signed MOS T-16 model device\n";

    printf("%s", menu);
}

struct
program_data
{
  char* device = nullptr;
  int fd;
  bool help = false;

} program_data;

static void
collect_options(int argc, char** argv)
{
  struct
  option long_options[]=
  {
    {"help"  , no_argument      , NULL, 'h'},
    {"device", required_argument, NULL, 'd'},
    {0, 0, 0, 0}
  };

  for(int i = 0; i < argc; i++)
  {
    auto input = getopt_long(argc, argv, ":hd:", long_options, nullptr);

    switch(input)
    {
      case -1:
        return;

      case '?':
        fprintf(stderr, "Unrecognized option: %s\n", argv[optind - 1]);
      break;

      case ':':
        fprintf(stderr, "Missing option to argument: %s\n", argv[optind - 1]);
      break;

      case 'h':
        program_data.help = true;
      break;

      case 'd':
        program_data.device = optarg;
      break;
    }
  }
}

static void
open_device()
{
  const char* const default_device = "/dev/serial/by-id/usb-ZEPHYR_building_model_E66098F29B1AA239-if00";

  if(program_data.device == nullptr)
    program_data.fd = open(default_device, O_RDWR);
  else
    program_data.fd = open(program_data.device, O_RDWR);
}

int
main(int argc, char** argv)
{
  collect_options(argc, argv);

  if(argc < 2 || program_data.help)
  {
    show_menu_options();
    return 0;
  }

  open_device();

  if(program_data.fd == -1)
  {
    perror("Device error: ");
    return 1;
  }

  for(; optind < argc; optind++)
  {
    std::string_view piwo7 = argv[optind];

    bool correct_piwo7_extension = piwo7.ends_with(".piwo7");

    if(!correct_piwo7_extension)
    {
      fprintf(stderr, "Path: %s is incorrect, file extension is not *.piwo7\n", piwo7.data());
      fprintf(stderr, "Skipping %s\n", piwo7.data());
      continue;
    }

    // -1 for '\0', -5 for 'piwo7', thus -6
    const auto extension_index = piwo7.length() - 6;

    char* mp3 = argv[optind];

    // start with +1 because extension_index points at '.' (i.e. dot visible at '*.<extension_name>')

    mp3[extension_index + 1] = 'm';
    mp3[extension_index + 2] = 'p';
    mp3[extension_index + 3] = '3';
    mp3[extension_index + 4] = '\0';
    mp3[extension_index + 5] = '\0';
  }

  return 0;
}
